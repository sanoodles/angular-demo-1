import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Score } from '../models/score';

@Injectable()
export class ScoreService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private scoresUrl = 'https://angularjs-demo-1.firebaseio.com/scores/highscores.json';  // URL to web api

  constructor(private http: Http) { }

  getScores(): Promise<Score[]> {
    return this.http.get(this.scoresUrl)
               .toPromise()
               .then(response => (<any>Object).values(response.json()) as Score[])
               .catch(this.handleError);
  }

  save(score: Score): Promise<any> {
    return this.http
               .post(this.scoresUrl, JSON.stringify(score), { headers: this.headers })
               .toPromise()
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}

