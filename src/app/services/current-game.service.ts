import { Injectable } from '@angular/core';

import { GameState }          from '../models/game-state';
import { SortedWordsService } from './sorted-words.service';

@Injectable()
export class CurrentGameService {
  private readonly gameDurationInSeconds: number = 40;
  private timer: any;
  state: GameState;
  onTimeIsUp: any = () => {};

  constructor(
    private sortedWordsService: SortedWordsService
  ) {
    this.state = new GameState();
  }

  reset(): Promise<void> {
    this.state.name = '';
    return this.sortedWordsService.getWords()
               .then((sortedWords) => {
                  this.shuffleArray(sortedWords);
                  this.state.sortedWords = sortedWords;
                  this.state.sortedWordsIterator = 0;
                  this.state.unsortedWord = this.getUnsortedWord(this.getSolution());
                  this.state.enteredWord = '';
                  this.state.currentReward = this.getRewardForWord(this.getSolution());
                  this.state.score = 0;
                  this.state.timeLeft = this.gameDurationInSeconds;
               });
  }

  start(): void {
    this.timer = setInterval(() => {
      this.state.timeLeft--;
      if (this.state.timeLeft < 1) {
        clearInterval(this.timer);
        this.onTimeIsUp();
      }
    }, 1000);
  }

  nextWord(): void {
    this.state.sortedWordsIterator++;
    this.state.unsortedWord = this.getUnsortedWord(this.getSolution());
    this.state.enteredWord = '';
    this.state.currentReward = this.getRewardForWord(this.getSolution());
  }

  getSolution(): string {
    return this.state.sortedWords[this.state.sortedWordsIterator];
  }

  end(): void {
    clearInterval(this.timer);
  }

  private shuffleArray(a: string[]): void {
    var j, x, i, n = a.length;
    for (i = n; i; i--) {
      j = Math.floor(Math.random() * i);
      x = a[i - 1];
      a[i - 1] = a[j];
      a[j] = x;
    }
  }

  private getUnsortedWord(word: string): string {
    var a = word.split("");
    this.shuffleArray(a);
    return a.join("");
  }

  private getRewardForWord(word: string): number {
    return Math.floor(Math.pow(1.95, word.length / 3));
  }

}

