import { Injectable }    from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class SortedWordsService {

  private wordsUrl = 'https://angularjs-demo-1.firebaseio.com/sorted-words/words-for-a-game.json';  // URL to web api

  constructor(private http: Http) { }

  getWords(): Promise<string[]> {
    return this.http.get(this.wordsUrl)
               .toPromise()
               .then(response => (<any>Object).values(response.json()) as string[])
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}

