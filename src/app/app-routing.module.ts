import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { HighscoreListComponent } from './highscore-list/highscore-list.component';
import { NewGameComponent }       from './new-game/new-game.component';
import { GameComponent }          from './game/game.component';

const routes: Routes = [
  { path: '', redirectTo: '/highscore-list', pathMatch: 'full' },
  { path: 'highscore-list',   component: HighscoreListComponent },
  { path: 'new-game',         component: NewGameComponent },
  { path: 'game',             component: GameComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

