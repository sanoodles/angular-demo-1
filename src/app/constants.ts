export const keyCodes = {
  backspace:  8,
  enter:      13,
  delete:     46,
}

export const colors = {
  success:  '#8f8',
  failure:  '#f88',
}

export const notificationPeriodInMs = 150;

