import { Component, OnInit }  from '@angular/core';
import { Router }             from '@angular/router';

import { GameState }          from '../models/game-state';
import { CurrentGameService } from '../services/current-game.service';

@Component({
  selector: 'new-game',
  templateUrl: './new-game.component.html',
})
export class NewGameComponent {
  state: GameState;
  isResetDone: boolean = false;

  constructor(
    private router: Router,
    private currentGameService: CurrentGameService,
  ) { }

  ngOnInit(): void {
    this.state = this.currentGameService.state;
    this.currentGameService.reset()
        .then(() => this.isResetDone = true);
  }

  startGame(): void {
    this.router.navigateByUrl('/game');
    this.currentGameService.start();
  }

  onKeyPress(event: any): void {
    if (event.keyCode == 13 &&
        this.isResetDone &&
        this.state.name.length > 0) {
      this.startGame();
    }
  }

}

