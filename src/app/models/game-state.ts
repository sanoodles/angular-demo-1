export class GameState {
  name: string;
  sortedWords: string[];
  sortedWordsIterator: number;
  unsortedWord: string;
  enteredWord: string;
  currentReward: number;
  score: number;
  timeLeft: number; // in seconds
}

