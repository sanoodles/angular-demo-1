import { NgModule }               from '@angular/core';
import { BrowserModule }          from '@angular/platform-browser';
import { FormsModule }            from '@angular/forms';
import { HttpModule }             from '@angular/http';

import { AppRoutingModule }       from './app-routing.module';

import { AppComponent }           from './app.component';
import { HighscoreListComponent } from './highscore-list/highscore-list.component';
import { NewGameComponent }       from './new-game/new-game.component';
import { GameComponent }          from './game/game.component';
import { ScoreService }           from './services/score.service';
import { CurrentGameService }     from './services/current-game.service';
import { SortedWordsService }     from './services/sorted-words.service';

@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    HighscoreListComponent,
    NewGameComponent,
    GameComponent,
  ],
  providers:    [
    ScoreService,
    CurrentGameService,
    SortedWordsService,
  ],
  bootstrap:  [ AppComponent ]
})
export class AppModule { }
